# EventMessageCenter

## 介绍

基于 JS 发布订阅模式实现的一个事件消息中心

## 博客

https://hunter1024.blog.csdn.net/article/details/124452792

## 调试说明

1.  pnpm i （依赖安装）
2.  pnpm build（构建）
3.  pnpm example（调试源码）
4.  example 文件夹下是使用 demo
5.  在 esm，cjs，window 环境下导入该工具库

## 使用说明

### 安装

`npm install event-message-center`
或
`yarn add event-message-center`
或
`pnpm install event-message-center`

### 引入

#### ESM

```javascript
import { messageCenter } from "event-message-center";
```

#### CJS

```javascript
const { messageCenter } = require("event-message-center");
```

#### 浏览器中

```html
<script src="./node_modules/event-message-center/dist/umd/index.js"></script>
<script>
  console.log(MessageCenter);
</script>
```

### 用法

#### 创建实例

```javascript
const bus = new MessageCenter();
```

#### 直接使用全局的单例

```javascript
const bus = messageCenter;
```

#### 注册事件至调度中心

```javascript
messageCenter.on("a", funcA);
```

#### 触发调度中心的某个或者某些该事件类型下注册的函数

```javascript
messageCenter.emit("a", { state: "stop" });
```

#### 销毁监听

```javascript
messageCenter.un("a", funcA);
messageCenter.un("a");
```

#### 只注册一次监听，执行即销毁

```javascript
messageCenter.once("a", funcA);
```

#### 重置调度中心

```javascript
messageCenter.clear();
```

#### 判断事件是否被订阅

```javascript
messageCenter.has("a");
```

#### 同一个事件被绑定了多少函数

```javascript
messageCenter.handlerLength("a");
```

#### 监听 invoke 的消息，若 handler 中进行了计算或者异步操作，会反馈给 invoke

```javascript
messageCenter.watch("b", funcB);
```

#### 触发 watch 事件，并且接收 watch 处理结果

```javascript
messageCenter.invoke("b", { num1: 1, num2: 2 }).then((result) => {
  console.log(result);
});
```

#### 构造函数 options

```javascript
const messageCenter2 = new MessageCenter({ blackList: ["c"], maxLen: 2 });
```

##### 黑名单

```javascript
messageCenter2.on("c", () => {}); // c is not allow
```

##### 限制事件个数

```javascript
messageCenter2.on("b", () => {});
messageCenter2.on("b", () => {});
messageCenter2.on("b", () => {}); // the number of b must be less than 2
```
