interface Handlers {
    [key: string]: Array<Function>
}
type IOptions = {
    blackList?: string[] // 黑名单，过滤功能
    maxLen?: number // 每种队列最大长度，限制事件个数
}
export class MessageCenter {
    protected events: Handlers = Object.create(null)
    constructor(public readonly options: IOptions = {}) { }
    /**
     * 注册事件至调度中心
     * @param type 事件类型，特指具体事件名
     * @param handler 事件注册的回调
     */
    on(type: string, handler: Function) { // 订阅者
        this.checkHandler(type, handler)
        const fns = this.getHandler(type, [])
        fns.push(handler)
        return this
    }
    /**
     * 触发调度中心的某个或者某些该事件类型下注册的函数
     * @param type 事件类型，特指具体事件名
     * @param data 发布者传递的参数
     */
    emit(type: string, data?: any) { //发布者
        if (this.has(type)) {
            this.runHandler(type, data)
        }
        return this
    }
    //销毁监听
    un(type: string, handler?: Function) {
        this.unHandler(type, handler)
        return this
    }
    // 只注册一次监听，执行即销毁
    once(type: string, handler: Function) {
        this.checkHandler(type, handler)
        const fn = (...args) => {
            this.un(type, fn);
            return handler(...args)
        }
        this.on(type, fn)
        return this
    }
    // 重置调度中心
    clear() {
        this.events = Object.create(null)
        return this
    }
    // 判断事件是否被订阅
    has(type: string) {
        return !!this.getHandler(type)
    }
    // 获取当前事件的所有函数，空数据可兼容
    getHandler(type: string, defaultVal?: any[]) {
        typeof defaultVal === "object" && (this.events[type] = this.events[type] ?? defaultVal)
        return this.events[type]
    }
    // 同一个事件被绑定了多少函数
    handlerLength(type: string) {
        return this.getHandler(type)?.length ?? 0
    }
    // 监听invoke的消息，若handler中进行了计算或者异步操作，会反馈给invoke
    watch(type: string, handler: Function) {
        this.checkHandler(type, handler)
        const fn = (...args) => {
            this.emit(this.prefixStr(type), handler(...args));
        }
        this.on(type, fn);
        return this
    }
    // 触发watch事件，并且接收watch处理结果
    invoke(type: string, data?: any) {
        return new Promise<void>((resolve) => {
            this.once(this.prefixStr(type), resolve);
            this.emit(type, data);
        })
    }
    // 批量执行调度中心中某个函数集
    private runHandler(type: string, data?: any) {
        const _fns = this.getHandler(type)
        for (const fn of _fns) {
            fn && fn(data)
        }
    }
    // 批量销毁调度中心中某个函数集
    private unHandler(type: string, handler: Function) {
        const _fns = this.getHandler(type)
        !handler && (this.events[type] = [])
        handler && this.checkHandler(type, handler)
        for (let i = 0; i < _fns.length; i++) {
            if (_fns && _fns[i] === handler) {
                _fns[i] = null
            }
        }
    }
    private prefixStr(str: string) {
        return `@${str}`
    }
    /**
     * 检查参数是否符合标准
     * @param type 事件名
     * @param handler 事件钩子
     */
    private checkHandler(type: string, handler: Function) {
        const { blackList = [], maxLen = Infinity } = this.options
        const len = this.handlerLength(type)
        if (type?.length === 0) {
            throw new Error('type.length can not be 0')
        }
        if (!handler || !type) {
            throw new ReferenceError('type or handler is not defined')
        }
        if (typeof handler !== 'function' || typeof type !== 'string') {
            throw new TypeError(`${handler} is not a function or ${type} is not a string`);
        }
        if (blackList.includes(type)) {// 事件类型黑名单
            throw new Error(`${type} is not allow`)
        }
        if (maxLen <= len) { // 限制事件个数
            throw new Error(`the number of ${type} must be less than ${maxLen}`)
        }
    }
    //返回当前类的实例的单例
    static Instance(Fn) {
        if (!Fn._instance) {
            Object.defineProperty(Fn, "_instance", {
                value: new Fn()
            });
        }
        return Fn._instance;
    }
}

export const messageCenter: MessageCenter = MessageCenter.Instance(MessageCenter)
export const decoratorMessageCenter: ClassDecorator = <TFunction extends Function>(target: TFunction) => {
    if (!target.prototype.messageCenter) {
        target.prototype.messageCenter = new MessageCenter()
    }
    return target
}
export default MessageCenter;