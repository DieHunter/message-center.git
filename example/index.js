/*
 * @Author: Hunter
 * @Date: 2022-04-15 14:33:05
 * @LastEditTime: 2023-03-26 12:16:44
 * @LastEditors: Hunter
 * @Description:
 * @FilePath: \message-center\example\index.js
 * 可以输入预定的版权声明、个性签名、空行等
 */
const { messageCenter, MessageCenter } = require("../dist/cjs");
const { Example } = require("./example");

new Example(messageCenter, MessageCenter);
