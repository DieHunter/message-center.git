/*
 * @Author: Hunter
 * @Date: 2022-04-27 11:10:07
 * @LastEditTime: 2023-03-26 13:47:34
 * @LastEditors: Hunter
 * @Description:
 * @FilePath: \message-center\example\example.js
 * 可以输入预定的版权声明、个性签名、空行等
 */
const funcC = async (args) => {
  await syncFn();
  return args.reduce((pre, next) => (pre += next));
};
// 异步函数
const syncFn = () => {
  return new Promise((res) => {
    setTimeout(res, 1000);
  });
};
class Example {
  constructor(messageCenter, MessageCenter) {
    // on
    messageCenter.on("a", funcA);
    // emit
    messageCenter.emit("a", { state: "emit" });
    // un
    messageCenter.un("a", funcA);
    messageCenter.un("a");
    messageCenter.emit("a", { state: "un" });
    // once
    messageCenter.once("a", funcA);
    messageCenter.emit("a", { state: "once" });
    // clear
    messageCenter.clear();
    messageCenter.emit("a", { state: "once" });
    // has
    messageCenter.has("a");
    // handlerLength
    messageCenter.handlerLength("a");
    // watch
    messageCenter.watch("c", funcC);
    // invoke
    messageCenter.invoke("c", [1, 2, 3, 4]).then((result) => {
      console.log(result);
    });
    // options
    const messageCenter2 = new MessageCenter({ blackList: ["c"], maxLen: 2 });
    // 黑名单
    messageCenter2.on("c", () => {}); // c is not allow
    // 限制事件个数
    messageCenter2.on("b", () => {});
    messageCenter2.on("b", () => {});
    messageCenter2.on("b", () => {}); // the number of b must be less than 2
  }
}

if (typeof define === "function" && define.amd) {
  // amd环境
  define(Example);
} else if (typeof exports === "object") {
  // cmd环境
  exports.Example = Example;
} else if (Window) {
  // 浏览器环境下
  window.Example = Example;
}
function funcA(args) {
  console.log(args);
}
function funcB(args) {
  const { num1, num2 } = args;
  //   console.log(args);
  return num1 + num2;
}
