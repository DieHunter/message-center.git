import typescript from "@rollup/plugin-typescript";
import json from "@rollup/plugin-json";
import { readFileSync } from "fs";
const packageJson = JSON.parse(readFileSync("./package.json", "utf8"));
const pkgName = packageJson.umdModuleName;
export default {
  input: "src/index.ts",
  output: {
    file: "dist/umd/index.js",
    format: "umd",
    name: pkgName,
  },
  plugins: [
    typescript({
      tsconfig: "./tsconfig.umd.json",
    }),
    json(),
  ],
};
