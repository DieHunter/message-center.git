# EventMessageCenter

## Introduction

An event message center based on JS publish and subscribe model

## Blog

https://hunter1024.blog.csdn.net/article/details/124452792

## Debugging instructions

1. pnpm i (dependent installation)
2. pnpm build
3. pnpm example (debugging source)
4. demo is used in the example folder
5. Import the tool library in esm, cjs, and window environments

## Instructions for use

### Install

`npm install event-message-center`
or
`yarn add event-message-center`
or
`pnpm install event-message-center`

### Introduce

#### ESM

```javascript
import { messageCenter } from "event-message-center";
```

#### CJS

```javascript
const { messageCenter } = require("event-message-center");
```

#### in your browser

```html
<script src="./node_modules/event-message-center/dist/umd/index.js"></script>
<script>
  console.log(MessageCenter);
</script>
```

### Usage

#### Create an instance

```javascript
const bus = new MessageCenter();
```

#### uses global singletons directly

```javascript
const bus = messageCenter;
```

#### Register the event with the dispatch center

```javascript
messageCenter.on("a", funcA);
```

#### fires one or more functions registered for this event type in the dispatch center

```javascript
messageCenter.emit("a", { state: "stop" });
```

#### Destroy monitor

```javascript
messageCenter.un("a", funcA);
messageCenter.un("a");
```

#### Register only once to listen, execute and destroy

```javascript
messageCenter.once("a", funcA);
```

#### Resets the dispatch center

```javascript
messageCenter.clear();
```

#### Determines whether the event is subscribed

```javascript
messageCenter.has("a");
```

#### How many functions are bound to the same event

```javascript
messageCenter.handlerLength("a");
```

#### listens to invoke messages and feedbacks to invoke if computations or asynchronous operations are performed in handler

```javascript
messageCenter.watch("b", funcB);
```

#### triggers the watch event and receives the watch processing result

```javascript
messageCenter.invoke("b", { num1: 1, num2: 2 }).then((result) => {
  console.log(result);
});
```

#### constructor options

```javascript
const messageCenter2 = new MessageCenter({ blackList: ["c"], maxLen: 2 });
```

##### The Blacklist

```javascript
messageCenter2.on("c", () => {}); // c is not allow
```

##### Limits the number of events

```javascript
messageCenter2.on("b", () => {});
messageCenter2.on("b", () => {});
messageCenter2.on("b", () => {}); // the number of b must be less than 2
```
